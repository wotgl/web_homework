var http = require("http");
var url = require("url");

function onRequest(request, response) {
	var pathname = url.parse(request.url).pathname;
	console.log("Request for " + pathname + " received.");

	var url_parts = url.parse(request.url, true);
	var query = url_parts.query;

	response.writeHead(200, {"Content-Type": "text/plain"});
	response.write("Hello from " + pathname + "\nParameters:\t" + JSON.stringify(query));
	response.end();
}

http.createServer(onRequest).listen(3000);

console.log("Server is running");