a = 12;
b = 12;
c = 3;

foo(a, b, c);

/*
Enter values from browser:
a = promt("Enter a: ");
b = promt("Enter b: ");
c = promt("Enter c: ");
*/


function foo(a, b, c) {
	d = b * b - 4 * a * c;
	if (a != 0) {
		x1 = (-b + Math.sqrt(d)) / (2 * a);
		x2 = (-b - Math.sqrt(d)) / (2 * a);
	} else {
		x1= -c / b;
		x2 = NaN;
	}

	if (x1 && x2) {
		console.log("answer =\t" + x1 + "\tand\t " + x2);
	} 
	else if (x1) {
		console.log("answer =\t" + x1);
	} else {
		console.log("not answer");
	}
}